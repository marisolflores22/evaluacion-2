class User < ActiveRecord::Base
    validates :firstname, presence:true, length: { minimum:3, maximum:50 }
    validates :lastname, presence:true, length: { minimum:3, maximum:50 }
    validates :email, presence:true, length: { minimum:3, maximum:60 }
end